from Kapture.app.ocr import KeitoOcr
from Kapture.app.ocr import ocr_func, key_detection
import multiprocessing
from PIL import Image
import pytesseract
from Kapture.app.ocr.ocr import BoxData
from Kapture.app.ocr.boxdet import Box
from Kapture.app.ocr.boxdet import box_detection, box_helpers
from Kapture.app.utilities import CONSTANTS
from Kapture.app.utilities.extension_helper import correct_skew
import cv2
import pandas as pd

BOX_FLAG = 'box_flg'

class BoxedFormExtractor(KeitoOcr):
    def __init__(self,image_path, key_path, manager, config_path=None):
        # def __init__(self, image_path, key_path, manager, config_path=None):
        # super().__init__(image_path, key_path, manager, config_path=config_path)

        self.lock = manager.Lock()
        setattr(CONSTANTS, "config", self.Config(config_path))
        self.img_path = image_path
        self.img_color = cv2.imread(image_path)
        # self.img_color = self.get_standard(self.img_color)  # crop to standard
        cv2.imwrite(image_path, self.img_color)
        if hasattr(CONSTANTS.config, 'skew_correction'):
            if CONSTANTS.config.skew_correction:
                self.img_color = correct_skew(self.img_color)
        self.img_gray = cv2.cvtColor(self.img_color.copy(), cv2.COLOR_BGR2GRAY)
        if hasattr(CONSTANTS.config, 'binary_method'):
            if CONSTANTS.config.binary_method == 'thresh':
                (t, self.img_binary) = cv2.threshold(self.img_gray, CONSTANTS.config.binary_thresh, 255,
                                                     cv2.THRESH_BINARY)
        else:
            self.img_binary = cv2.adaptiveThreshold(
                self.img_gray.copy(),
                255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                cv2.THRESH_BINARY, 15, 4
            )

        if CONSTANTS.config.box_detection[BOX_FLAG]:
            self.boxes = box_detection.Box.get_boxes(self.img_binary, **CONSTANTS.config.box_detection)
        self.final_boxes = manager.dict()
        self.height, self.width = self.img_gray.shape
        self.final_key_value = manager.dict()
        self.raw = pd.read_excel(key_path)
        self.all_keys = manager.list(self.raw.KEYS)
        self.all_keys1 = manager.list(self.raw.KEYS)
        self.RC = manager.list([0] * len(self.all_keys))
        self.RL = manager.list([0] * len(self.all_keys))
        self.all_keys_original = manager.list(self.raw.KEYS)
        self.key_flags = manager.list(self.raw.BOXFLAG)
        self.key_max_words = manager.list([0] * len(self.all_keys))
        self.key_max_words1 = manager.list([0] * len(self.all_keys))
        self.display_keys = manager.list(self.raw.KEYS)
        self.bottom_values = manager.list()
        self.tables = manager.list()
        self.table_new = manager.list()
        self.symbol = manager.list()
        self.symbol.extend(
            ['.', '*', '|', '+', '?', '{', '}', ',', '=', '#', '(', ')', '[', ']', '-', '^', '$', '!', '<', '>'])

        # self.is_standard = self.check_for_fallback()  # check if image is similar to standard


    def thread(self,i):
        word_id = 0
        cropped_img_gray, cropped_img_binary = self.get_new_image(i)
        inside_boxes = Box.get_check_boxes(cropped_img_binary, **CONSTANTS.config.checkbox_detection)
        inside_boxes = box_helpers.remove_small_boxes(inside_boxes)
        new_cropped, all_checkboxes = self.process_check_boxes(
            inside_boxes, self.img_binary, cropped_img_gray, self.boxes[i],
            **CONSTANTS.config.checkbox_detection
        )
        im_pil = Image.fromarray(new_cropped)
        text = pytesseract.image_to_string(im_pil)
        text_dict = pytesseract.image_to_data(im_pil, output_type=pytesseract.Output.DICT)
        for j in range(len(text_dict['text'])):
            if text_dict['text'][j].strip() is not "":
                z = self.final_boxes[i]
                z.word_params[word_id] = BoxData.WordParams(
                    word_id, text_dict['text'][j],
                    self.boxes[i].xmin + text_dict['left'][j],
                    self.boxes[i].ymin + text_dict['top'][j],
                    self.boxes[i].xmin + text_dict['left'][j] + text_dict['width'][j],
                    self.boxes[i].ymin + text_dict['top'][j] + text_dict['height'][j],

                )
                self.final_boxes[i] = z
                word_id += 1
        out = ocr_func.get_key_value(self.final_boxes[i].word_params, all_checkboxes,
                                     **CONSTANTS.config.checkbox_detection)
        keys, orig_keys = key_detection.get_keys_from_string(text, self.all_keys1, self.key_max_words1)
        self.get_key_value_by_flag(self.boxes[i], text, keys, orig_keys)


    def process_form(self, processes=int(multiprocessing.cpu_count() / 2)):
        pp = []
        for i in self.boxes:
            pp.append(i)
            # cv2.rectangle(self.img_color, (self.boxes[i].xmin, self.boxes[i].ymin),
            #               (self.boxes[i].xmax, self.boxes[i].ymax), color=(0,0,255), thickness=2)
            # cv2.putText(self.img_color, str(i), org=(self.boxes[i].xmin, self.boxes[i].ymax),
            #             fontScale=2, fontFace=cv2.FONT_HERSHEY_PLAIN, color=(255,0,0))
        pool = multiprocessing.Pool(processes=processes)
        pool.map(self.thread, pp)
        pool.close()
        pool.join()

        for i in self.bottom_values:
            temp_value = ""
            for j in i[2]:

                if self.final_boxes[j].text.strip() != "":
                    temp_value += self.final_boxes[j].text + ","
            self.final_key_value[i[0]] = {"key": i[1], "value": temp_value}
        final_keys, final_values, final_coords = self.get_final_key_and_value()
        return final_keys, final_values, final_coords

