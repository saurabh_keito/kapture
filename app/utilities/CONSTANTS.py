WEBSITE = "http://localhost:5000/"

BASE_PATH = "/home/user/PycharmProjects/kapture/Kapture/"
default_config = f"{BASE_PATH}app/resources/configs/default.json"
default_keys = f"{BASE_PATH}app/resources/default.xlsx"
savePath = f"{BASE_PATH}app/static/out"
