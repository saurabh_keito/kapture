import os
import time
from PIL import Image
from wand.image import Image as Image2, Color
from ..utilities import CONSTANTS
from fpdf import FPDF
import pandas as pd
import cv2
import numpy as np


def convert_pdf(pdf_path):
    save_path = CONSTANTS.savePath
    results = []
    with(Image2(filename=pdf_path, resolution=300)) as source:
        images = source.sequence
        pages = len(images)
        for i in range(pages):
            img = Image2(images[i])
            img.background_color = Color('white')  # Set white background.
            img.alpha_channel = 'remove'
            t = time.time()
            path = save_path + "/" + str(t) + ".png"
            img.save(filename=path)
            results.append(path)
    return results


def convert_tiff(image_path):
    save_path = CONSTANTS.savePath
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    img = Image.open(image_path)
    results = []
    for i in range(8):
        try:
            img.seek(i)
            t = time.time()
            img.save(save_path + '/%s.png' % (t,))
            results.append(save_path + "/" + str(t) + ".png")
        except EOFError:
            break
    return results


def correct_skew(img):

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    bit_wised = cv2.bitwise_not(img_gray)

    thresh = cv2.threshold(bit_wised, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    cords = np.column_stack(np.where(thresh > 0))
    angle_ = cv2.minAreaRect(cords)[-1]
    if angle_ < -45:
        angle_ = -(90 + angle_)
    else:
        angle_ = -angle_

    (h, w) = img_gray.shape[:2]

    centre = (w // 2, h // 2)
    matrix = cv2.getRotationMatrix2D(centre, angle_, 1.0)
    rotated = cv2.warpAffine(img, matrix, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

    return rotated


def convert_images_to_pdf(image_list: list, pdf_path):
    pdf = FPDF()
    for image in image_list:
        pdf.add_page()
        pdf.image(image, 0, 0, 210)
    pdf.output(pdf_path, "F")


def get_excel_path(data):
    """
    saves excel
    :param data: Edit values from validator
    :return: saved excel path
    """
    excel_file_path = CONSTANTS.savePath + "/" + str(time.time()) + ".xlsx"
    writer = pd.ExcelWriter(excel_file_path)
    for x in data:
        df = pd.DataFrame([data[x]["key"], data[x]["value"], data[x]["originalValue"]]).transpose()
        df.columns = ["Keys", "Edited Values", "Original Values"]
        df.to_excel(writer, x)
    writer.save()
    return excel_file_path


def get_csv_or_json_path(data, csv=True):
    a = []; b = []; c = [];
    for x in data:
        a.extend(data[x]["key"])
        b.extend(data[x]["value"])
        c.extend(data[x]["originalValue"])
    d = {"keys": a, "edited values": b, "original values": c}
    df = pd.DataFrame(d)
    if csv:
        csv_file_path = CONSTANTS.savePath + "/" + str(time.time()) + ".csv"
        df.to_csv(csv_file_path)
    else:
        csv_file_path = CONSTANTS.savePath + "/" + str(time.time()) + ".json"
        df.to_json(csv_file_path)
    return csv_file_path
