import xml.etree.ElementTree as ET


def xml_to_dict(xml_path):
    key_cord_dict = {}
    tree = ET.parse(xml_path)
    root = tree.getroot()

    for object_ in root.findall('object'):

        bb_box = object_.find('bndbox')
        key = object_.find('name').text
        cords = []
        for point in bb_box:
            cords.append(int(point.text))
        d = {key: cords}
        key_cord_dict.update(d)

    return key_cord_dict

# key_cord_dict = xml_to_dict("/home/user/Documents/Keito/form validation/Generic Fom validation/0001906176_1008.pdf.xml")