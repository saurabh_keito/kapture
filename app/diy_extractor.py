from Kapture.app.ocr import KeitoOcr
from Kapture.app.ocr import ocr_func, key_detection
import multiprocessing
from PIL import Image
import pytesseract
from Kapture.app.ocr.ocr import BoxData
from Kapture.app.ocr.boxdet import Box
from Kapture.app.ocr.boxdet import box_detection, box_helpers
from Kapture.app.utilities import CONSTANTS
from Kapture.app.utilities.extension_helper import correct_skew
import cv2
import pandas as pd
from yattag import Doc, indent
import xml.etree.ElementTree as ET

BOX_FLAG = 'box_flg'


class Diy_Extractor(KeitoOcr):

    def __init__(self, image_path, key_path, manager, config_path=None):
        # def __init__(self, image_path, key_path, manager, config_path=None):
            # super().__init__(image_path, key_path, manager, config_path=config_path)

        self.lock = manager.Lock()
        setattr(CONSTANTS, "config", self.Config(config_path))
        self.img_path = image_path
        self.img_color = cv2.imread(image_path)
        # self.img_color = self.get_standard(self.img_color)  # crop to standard
        cv2.imwrite(image_path, self.img_color)
        if hasattr(CONSTANTS.config, 'skew_correction'):
            if CONSTANTS.config.skew_correction:
                self.img_color = correct_skew(self.img_color)
        self.img_gray = cv2.cvtColor(self.img_color.copy(), cv2.COLOR_BGR2GRAY)
        if hasattr(CONSTANTS.config, 'binary_method'):
            if CONSTANTS.config.binary_method == 'thresh':
                (t, self.img_binary) = cv2.threshold(self.img_gray, CONSTANTS.config.binary_thresh, 255,
                                                     cv2.THRESH_BINARY)
        else:
            self.img_binary = cv2.adaptiveThreshold(
                self.img_gray.copy(),
                255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                cv2.THRESH_BINARY, 15, 4
            )

        if CONSTANTS.config.box_detection[BOX_FLAG]:
            self.boxes = box_detection.Box.get_boxes(self.img_binary, **CONSTANTS.config.box_detection)
        self.final_boxes = manager.dict()
        self.height, self.width = self.img_gray.shape
        self.final_key_value = manager.dict()
        # self.is_standard = self.check_for_fallback()  # check if image is similar to standard

    def get_standard_form(self):
        return True


    def extract_boxes(self):
        return self.boxes


    def get_words(self,processes=int(multiprocessing.cpu_count() / 2)):
        """

        :return: list of word, list of coord
        """

        pool = multiprocessing.Pool(processes=processes)
        pool.map(self.process_boxes, list(range(len(self.boxes))))
        pool.close()
        pool.join()
        word = []
        coord = []
        bounding_box = {}

        for box in sorted(self.final_boxes):
            if box:

                bounding_box[str(box)]= {"bbox":[self.final_boxes[box].box_params.xmin,
                                        self.final_boxes[box].box_params.ymin,
                                        self.final_boxes[box].box_params.xmax,
                                        self.final_boxes[box].box_params.ymax],"word":[],"coord":[]}
                for a in range(len(self.final_boxes[box].word_params)):
                    bounding_box[str(box)]["word"].append(self.final_boxes[box].word_params[a].word)
                    bounding_box[str(box)]["coord"].append([self.final_boxes[box].word_params[a].xmin,
                          self.final_boxes[box].word_params[a].ymin,
                          self.final_boxes[box].word_params[a].xmax,
                          self.final_boxes[box].word_params[a].ymax])

        # print(bounding_box)

        # print(xml)
        return bounding_box

    @staticmethod
    def to_xml(bb_dict):
        doc, tag, text = Doc().tagtext()
        with tag("root"):
            for num, key in enumerate(bb_dict.keys()):
                # print("key", key)
                with tag("key"+str(key)):
                    xmin, ymin, xmax, ymax = bb_dict[key]["bbox"]
                    with tag("bbox"):
                        text(xmin,",", ymin,",", xmax,",", ymax)
                        # with tag('xmin'):
                        #     text(xmin)
                        # with tag('ymin'):
                        #     text(ymin)
                        # with tag('xmax'):
                        #     text(xmax)
                        # with tag('ymax'):
                        #     text(ymax)

                    with tag("words"):
                        for num_1, [w, c] in enumerate(zip(bb_dict[key]["word"],bb_dict[key]["coord"])):
                            text(w+",")
                            text(",".join([str(cc) for cc in c ]))
                            text("\n")

        result = indent(
            doc.getvalue(),
            indentation=' ' * 4,
            newline='\r\n'
        )
        with open("word_coord.xml",'w') as f:
            f.write(result)

    @staticmethod
    def xml_to_dict(xml_path):
        key_cord_dict = {}
        tree = ET.parse(xml_path)
        root = tree.getroot()
        bounding_box = {}

        for key_num, key in enumerate(root):
            bounding_box[str(key_num)] = {"bbox":[],"words":[],"coord":[]}
            bbox = key[0]
            word = key[1]
            bounding_box[str(key_num)]["bbox"] = list(map(int,bbox.text.split(",")))
            text = word.text
            if text:
                text = text.split("\n")
                for tt in text:
                    tt = tt.split(',')
                    print(tt)
                    if tt[0]:
                        bounding_box[str(key_num)]["words"].append(tt[0])
                        bounding_box[str(key_num)]["coord"].append(list(map(int,tt[-4:])))
        return bounding_box











    def process_boxes(self,i):
        word_id = 0
        cropped_img_gray, cropped_img_binary = self.get_new_image(i)
        inside_boxes = Box.get_check_boxes(cropped_img_binary, **CONSTANTS.config.checkbox_detection)
        inside_boxes = box_helpers.remove_small_boxes(inside_boxes)
        new_cropped, all_checkboxes = self.process_check_boxes(
            inside_boxes, self.img_binary, cropped_img_gray, self.boxes[i],
            **CONSTANTS.config.checkbox_detection
        )
        im_pil = Image.fromarray(new_cropped)
        # text = pytesseract.image_to_string(im_pil)
        text_dict = pytesseract.image_to_data(im_pil, output_type=pytesseract.Output.DICT)
        for j in range(len(text_dict['text'])):
            if text_dict['text'][j].strip() is not "":
                z = self.final_boxes[i]
                z.word_params[word_id] = BoxData.WordParams(
                    word_id, text_dict['text'][j],
                    self.boxes[i].xmin + text_dict['left'][j],
                    self.boxes[i].ymin + text_dict['top'][j],
                    self.boxes[i].xmin + text_dict['left'][j] + text_dict['width'][j],
                    self.boxes[i].ymin + text_dict['top'][j] + text_dict['height'][j],

                )
                self.final_boxes[i] = z
                word_id += 1

    def collate_keys(self, key_value_dict):
        """
        :key_value_dict: {keys:{key1:[word,word,..],key2:[...]},values:{}}}
        :return:
        """
        key_dict = key_value_dict["keys"]
        final_keys = []
        for key in key_dict.keys():
            temp_key = " ".join(key_dict[key])
            final_keys.append(temp_key)

        return final_keys

    def get_keys_excel(self, key_value_dict):
        keys = self.collate_keys(key_value_dict)
        word_flags = ["W"]*len(keys)
        dict1 = {"KEYS": keys,
                 "BOXFLAG": word_flags}
        df = pd.DataFrame(data=dict1)
        print(df)
        df.to_excel("/home/user/PycharmProjects/kapture/Kapture/app/resources/demo_default.xlsx")

    class Standardization:
        def __init__(self):
            pass

        def get_standard(self, img):
            # cv2.namedWindow("original", cv2.WINDOW_NORMAL)
            original = img.copy()
            # cv2.imshow("original",original)
            ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY_INV)

            img_bin = Box.detect_lines(
                img.copy(), True, True,
                60, join_lines=True, iterations=1)
            # horizontal_lines_img = cv2.cvtColor(horizontal_lines_img, cv2.COLOR_BGR2GRAY)
            # ret, horizontal_lines_img = cv2.threshold(horizontal_lines_img, 10, 255,cv2.THRESH_BINARY_INV)
            # img, lines = Box.get_hough_lines(horizontal_lines_img, 30)
            # lines = np.array(lines)
            # # print(lines)
            # x1 = int(np.average(lines[:, 0][np.argsort(lines[:, 0])[:10]]))
            # y1 = int(np.average(lines[:, 1][np.argsort(lines[:, 1])[:10]]))
            # x2 = int(np.average(lines[:, 2][np.argsort(lines[:, 2])[-10:]]))
            # y2 = int(np.average(lines[:, 3][np.argsort(lines[:, 3])[-10:]]))
            #
            # # print(lines[:, 2][np.argsort(lines[:, 2])[-10:]])
            # # print(lines[:, 3][np.argsort(lines[:, 3])[-10:]])
            # print(x1, y1, x2, y2)
            # # cv2.rectangle(img, (x1, y1), (x2, y2), (255), -1)
            # cropped = original[y1:y2, x1:x2]
            # cropped = cv2.copyMakeBorder(cropped, 2, 2, 2, 2, cv2.BORDER_CONSTANT, None, 0)
            # cropped = cv2.copyMakeBorder(cropped, 2, 2, 2, 2, cv2.BORDER_CONSTANT, None, (255,255,255))
            standard = cv2.resize(img_bin, (2400, 3500))
            # cv2.imwrite()
            # cv2.namedWindow("img", cv2.WINDOW_NORMAL)
            # cv2.imshow("img", standard)
            # cv2.waitKey(0)
            return standard

        def process_boxes(self):
            pass


