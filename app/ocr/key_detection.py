"""key_detection
* @author: Aditya Aman
* Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
* This File is used for getting key by windowing and checking Levenshtein distance.
"""

from Kapture.app.utilities import CONSTANTS

keys = None
MAX_WORDS = None


def get_keys_from_string(raw, key, max_words, remove_original=True, max_key_length=18):
    """
    * The function to get extracted keys from a raw text.
    :param raw: The raw text
    :param key: The list of pre-defined keys
    :param max_words: the list of pre-defined max words
    * associated with the key
    :param remove_original: Default: True;
    * Should remove matched key from original list.
    :param max_key_length: The maximum length till which keys can be formed
                            by using the raw words
    :return:
    """
    data = raw.replace("\n", " ")
    data = data.split(" ")
    global keys
    global MAX_WORDS
    keys = key
    MAX_WORDS = max_words
    data = [s.rstrip() for s in data]
    _ = remove_empty(data)
    op = []
    orig = []
    i = 0
    while i < len(data):
        val, original, volume = find_key(i, data, [], remove_original, max_key_length)
        if val:
            op.append(val)
            orig.append(original)
            i += volume
            continue
        i += 1
    return op, orig


def remove_empty(arr):
    """
    * This function removes any empty element present in the array.
    :param arr: raw array
    :return: Cleaned array
    """
    if '' not in arr:
        return arr
    arr.remove('')
    return remove_empty(arr)


def find_key(start, arr, search, remove_original, max_key_length=18):
    """
    * A recursive function to search the key from raw text
    * by referencing a pre-filled key list.
    * The maximum length till which keys can be formed
    * by using the raw words is 18.
    :param start: The start index of the array ( The first word
    to be picked to form the key )
    :param arr: The raw text as array
    :param search: List of words to make a key.
    * ex: `No. of units` will be `["No.", "of", "units"]`
    :param remove_original: Remove keys after finding.
    :param max_key_length: The maximum length till which keys can be formed
                            by using the raw words
    :return: Tuple of `detected key`, `Key with it matched`,
    `number of words with which the key is formed.`
    """
    if len(search) > max_key_length:
        return None, None, None
    if len(search) == 0:
        search.append(arr[start])
    key, standard = extract_key(" ".join(search), arr, remove_original)
    if key:
        return key, standard, len(search)
    else:
        if len(search) + start < len(arr):
            search.append(arr[len(search) + start])
            return find_key(start, arr, search, remove_original, max_key_length)
        else:
            return None, None, None


def extract_key(item, raw, remove_original):
    """
    *
    :param item: The item to match.
    :param raw: The raw text
    :param remove_original: Remove original key if matched
    :return: Tuple of ( `Detected Key`, `Actual key in excel` )
    """
    key_len = [len(x) for x in keys]
    if len(item) in key_len:
        if item in keys and is_max_words(item, raw):
            i = keys.index(item)
            if remove_original:
                MAX_WORDS.pop(i)
                keys.remove(item)
            return item, item
        else:
            matching, orig = is_matching(get_keys_of_length(len(item)), item)
            if matching and is_max_words(orig, raw):
                i = keys.index(orig)
                if remove_original:
                    MAX_WORDS.pop(i)
                    keys.remove(orig)
                return item, orig
    return None, None


def is_max_words(orig, raw):
    """
    * The function is written to skip those extracted keys
    * who have restriction in length of raw text.
    * ex: `to` keyword can have restriction of raw text
    * length of 7 or 8 so do not pick key `to` if raw text is > 7.
    :param orig: Original Key
    :param raw: The raw text from which the key is extracted
    :return: Boolean value representing success or failure.
    """
    i = keys.index(orig)
    if MAX_WORDS[i] > 0:
        if len(raw) > MAX_WORDS[i]:
            return False
    return True


def is_matching(keys_similar, word):
    """
    *
    :param probability: The amount of matching filter
    :param keys_similar: List of keys with same length as word
    :param word: The input to match against the keys
    :return: Tuple with first value as Boolean flag for success
    and the second value as the detected key or None.
    """
    probability = CONSTANTS.config.key_check_probability
    for key in keys_similar:
        total = len(word)
        success = 1
        failure = 0
        for i in range(total):
            if success < probability:
                break
            if word[i].lower() != key[i].lower():
                failure = failure + 1/total
                success = 1 - failure
        if success > probability:
            return True, key
    return False, None


def get_keys_of_length(length):
    """
    * Creates a list of keys which have a specific length
    :param length: The length for which keys to gather
    :return: List of keys
    """
    to_send = []
    for k in keys:
        if len(k) == length:
            to_send.append(k)
    return to_send
