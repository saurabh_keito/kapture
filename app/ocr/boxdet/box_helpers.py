"""box_detection
* @author: Rajendraprasad Lawate
* Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
* This File contains helper functions for detecting boxs/checkboxes.
"""


import cv2
import math


def get_horizontal_kernel(kernel_length):
    return cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))


def get_vertical_kernel(kernel_length):
    return cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))


def get_kerneled_image(img, kernel, iterations, vert_dilate=None):
    img_temp2 = cv2.erode(img, kernel, iterations=iterations)
    if vert_dilate:
        iterations = vert_dilate
    return cv2.dilate(img_temp2, kernel, iterations=iterations)


def sort_contours(cnts, method="left-to-right"):
    """

    :param cnts: list of contours
    :param method: method of sorting
    :return: sorted contours and bounding boxes
    """
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    # construct the list of bounding boxes and sort them from top to
    # bottom
    bounding_boxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, bounding_boxes) = zip(*sorted(zip(cnts, bounding_boxes), key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return cnts, bounding_boxes


def get_slope(x1, y1, x2, y2):
    """
    :param x1: x co-ordinate of 1st point
    :param y1: y co-ordinate of 1st point
    :param x2: x co-ordinate of 2nd point
    :param y2: y co-ordinate of 2nd point
    :return: slope(in degrees) between 2 points
    """
    return math.degrees(math.atan((y2-y1)/(x2-x1+0.000001)))


def join_consecutive_lines(img, join_contour_count=6):
    """
    joins 2 consecutive horizontal lines if angle between there endpoints is around 90 degrees
    :param img: binary image with horizontal lines only
    :return: img with horizontal lines joined if met conditions
    """
    contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 1:
        (contours, boundingBoxes) = sort_contours(contours, method="top-to-bottom")
        height, width = img.shape
        width_range = int(width * (30 / 100))
        contour_list = []
        for c in contours:
            x0, y0, w0, h0 = cv2.boundingRect(c)
            if w0 > width_range:
                contour_list.append([x0, y0, w0, h0])
        for c in range(len(contour_list)):
            x0, y0, w0, h0 = contour_list[c][0], contour_list[c][1], contour_list[c][2], contour_list[c][3]
            brk_flg1 = False
            brk_flg2 = False
            count = 0
            for cx in range(c, len(contour_list)):

                x1, y1, w1, h1 = contour_list[cx][0], contour_list[cx][1], contour_list[cx][2], contour_list[cx][3]
                angle1 = get_slope(x1, y1, x0, y0)
                angle2 = get_slope(x1 + w1, y1 + h1, x0 + w0, y0 + h0)
                if 86 < abs(angle1) < 94 and not brk_flg1:
                    cv2.line(img, (x0, y0), (x1, y1), 255, thickness=2)
                    brk_flg1 = True
                elif 86 < abs(angle2) < 94 and not brk_flg2:
                    cv2.line(img, (x0 + w0, y0 + h0), (x1 + w1, y1 + h1), 255, thickness=2)
                    brk_flg2 = True
                if c == cx:
                    continue
                elif count == join_contour_count:
                    break
                count += 1

    return img


def remove_duplicate_boxes(boxes):

    temp_boxes1 = boxes.copy()

    for i in boxes:
        xmin = boxes[i].xmin
        ymin = boxes[i].ymin
        xmax = boxes[i].xmax
        ymax = boxes[i].ymax
        area = boxes[i].area
        for j in boxes:
            if i != j:
                area1 = boxes[j].area
                centroidx = boxes[j].centroid_x
                centroidy = boxes[j].centroid_y
                if xmin < centroidx < xmax and ymin < centroidy < ymax and area1 < area and (area1 / area) > 0.8:
                    del temp_boxes1[i]
    keys = list(temp_boxes1.keys())
    for i in keys:
        temp_boxes1[i] = temp_boxes1.pop(i)


    return temp_boxes1


def remove_small_boxes(boxes):
    temp_boxes1 = boxes.copy()
    for i in boxes:
        xmin = boxes[i].xmin
        ymin = boxes[i].ymin
        xmax = boxes[i].xmax
        ymax = boxes[i].ymax
        area = boxes[i].area
        for j in boxes:
            if i != j:
                area1 = boxes[j].area
                centroidx = boxes[j].centroid_x
                centroidy = boxes[j].centroid_y
                if xmin < centroidx < xmax and ymin < centroidy < ymax and area1 < area:
                    if i in temp_boxes1:
                        del temp_boxes1[i]
    keys = list(temp_boxes1.keys())
    for i in keys:
        temp_boxes1[i] = temp_boxes1.pop(i)

    return temp_boxes1
