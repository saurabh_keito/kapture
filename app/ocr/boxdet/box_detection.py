"""box_detection
* @author: Rajendraprasad Lawate
* Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
* This File's function are used for detecting boxs/checkboxes.
"""

from .box_helpers import *
import numpy as np


class Box:

    def __init__(self, xmin, ymin, xmax, ymax, box_id=None, area=None):
        self.box_id = box_id
        self.value = None
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.centroid_x = int(xmin + ((xmax - xmin) / 2))
        self.centroid_y = int(ymin + ((ymax - ymin) / 2))
        if area:
            self.area = area
        else:
            self.area = (xmax - xmin) * (ymax - ymin)

    def __str__(self):
        data = 'Box, {}, {}, {}, {}, {}'.format(self.xmin, self.ymin, self.xmax, self.ymax, self.centroid_x,
                                                self.centroid_y, self.box_id)
        return data

    def get_type(self):
        return 1, self.xmin, self.ymin, self.xmax, self.ymax, self.box_id, self.value

    @staticmethod
    def detect_radio_button(img, dp, min_dist, sensitivity, edge_points, min_radius, max_radius, shrink_percent, **kwargs):
        '''
        This function is used to detect if any circular radio button is available in provided gray-scale image.
        :param img: Gray-scale cropped image
        :param dp: Inverse ratio of the accumulator resolution to the image resolution. For example, if
                   dp=1 , the accumulator has the same resolution as the input image. If dp=2 , the accumulator has
                   half as big width and height.
        :param min_dist: Minimum distance between the centers of the detected circles. If the parameter is
                        too small, multiple neighbor circles may be falsely detected in addition to a true one. If it is
                        too large, some circles may be missed.
        :param sensitivity: In case of #HOUGH_GRADIENT , it is the higher
                            threshold of the two passed to the Canny edge detector
        :param edge_points: In case of #HOUGH_GRADIENT , it is the
                            accumulator threshold for the circle centers at the detection stage. The smaller it is, the more
                            false circles may be detected.
        :param min_radius: Minimum circle radius.
        :param max_radius: Maximum circle radius. If <= 0, uses the maximum image dimension. If < 0, returns
                           centers without finding the radius
        :param shrink_percent: box to be shrink to avoid outer order of circle
        :param kwargs:
        :return: Box object- with detected radio button coordinates
        '''

        circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, dp, min_dist,
                                   param1=sensitivity, param2=edge_points,
                                   minRadius=min_radius, maxRadius=max_radius)
        if circles is not None:
            # convert the (x, y) coordinates and radius of the circles to integers
            circles = np.round(circles[0, :]).astype("int")
            # loop over the (x, y) coordinates and radius of the circles
        coordinates = {}
        count = 0
        if not isinstance(circles, type(None)):
            for (x, y, r) in circles:
                xmin = x - r + 6
                ymin = y - r + 6
                xmax = x + r - 6
                ymax = y + r - 6
                width = xmax - xmin
                height = ymax - ymin
                area = width * height
                coordinates[count] = Box(xmin=xmin, ymin=ymin, xmax=xmax,
                                          ymax=ymax, box_id=count, area=area)
                count += 1
        return coordinates

    @staticmethod
    def get_check_boxes(img, kernel_length, approx_poly_percent=0.01,
                        upper_limit=1.5, lower_limit=0.5, min_area=0,
                        max_area=None, iterations=2, **kwargs):
        """
        This function gets the check boxes acc to the parameters passed
        :param img: the binary image that needs to be processed
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param approx_poly_percent: how similar can the contour be for the particular shape
        :param upper_limit: upper limit for height/width
        :param lower_limit: lower limit for height/width
        :param min_area: min area of a box i.e height * width
        :param max_area: min area of a box i.e height * width
        :param iterations: no of time erosion and dilation will occur
        :return: list of box objects
        """
        vertical_lines_img = Box.get_vertical_lines(img, kernel_length, iterations)
        horizontal_lines_img = Box.get_horizontal_lines(img, kernel_length, iterations)
        img_final_bin = cv2.bitwise_or(vertical_lines_img, horizontal_lines_img)
        contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        boxes = {}
        box_id = 0
        for c in contours:

            x, y, w, h = cv2.boundingRect(c)
            peri = cv2.arcLength(c, True)
            area = h*w
            approx = cv2.approxPolyDP(c, approx_poly_percent * peri, True)
            if upper_limit > (h / w) > lower_limit and len(approx) == 4:
                if area > min_area:
                    if max_area:
                        if area < max_area:
                            boxes[box_id] = (Box(xmin=x, ymin=y, xmax=x + w, ymax=y + h,
                                                 box_id=box_id))
                            box_id += 1
                    else:
                        boxes[box_id] = (Box(xmin=x, ymin=y, xmax=x + w, ymax=y + h,
                                             box_id=box_id))
                        box_id += 1

        return boxes

    @staticmethod
    def get_boxes(img, vert_flg=False, horz_flg=False, kernel_length=None, join_lines=False, iterations=2, skip_area_ratio=0.7,join_contour_count=6, **kwargs):
        """
        :param img: binary image
        :param vert_flg: for joining small breaks in vertical lines
        :param horz_flg: for joining small breaks in horizontal lines
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param join_lines: joins 2 consecutive horizontal line
        if angle between there endpoints is around 90 degrees if True
        :param iterations: iterations of erosion and dilation
        :return: dict of detected box objects
        """

        if "vert_dilate" in kwargs:
            vert_dilate = kwargs['vert_dilate']
        else:
            vert_dilate = None

        img_final_bin = Box.detect_lines(
            img.copy(), vert_flg, horz_flg,
            kernel_length, join_lines=join_lines, iterations=iterations, vert_dilate=vert_dilate,
            join_contour_count=join_contour_count
        )
        height, width = img.shape
        contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        if contours:
            (contours, boundingBoxes) = sort_contours(contours, method="top-to-bottom")
        else:
            return

        temp_boxes = {}
        idx = 0
        total_area = height * width
        for c in contours:
            if (cv2.contourArea(c) / total_area) > 0.0002:
                x, y, w, h = cv2.boundingRect(c)
                new_area = w * h
                area_ratio = new_area / total_area
                if area_ratio < skip_area_ratio:
                    temp_boxes[idx] = Box(x, y, x + w, y + h, idx, new_area)
                    idx += 1
        return temp_boxes

    @staticmethod
    def get_horizontal_lines(img, kernel_length=None, iterations=2):
        """
        this functions returns the horizontal lines
        :param img: input binary image
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param iterations: iterations of erosion and dilation
        :return: image convolved with kernel
        """
        if not kernel_length:
            kernel_length = np.array(img).shape[1] // 80
        kernel = get_horizontal_kernel(kernel_length)
        return get_kerneled_image(img, kernel, iterations)

    @staticmethod
    def get_vertical_lines(img, kernel_length=None, iterations=2, vert_dilate=None):
        """
        this functions returns the vertical lines
        :param img: input binary image
        :param kernel_length: the kernel length acc to which the border's minimum length will be decided
        :param iterations: iterations of erosion and dilation
        :return:
        """
        kernel = get_vertical_kernel(kernel_length)
        return get_kerneled_image(img, kernel, iterations, vert_dilate)

    @staticmethod
    def detect_lines(img, vert_flg=False, horz_flg=False, kernel_length=None, join_lines=False, iterations=2,
                     vert_dilate=None, join_contour_count=6):

        # Invert image
        img = 255 - img
        vertical_lines_img = Box.get_vertical_lines(img, kernel_length, iterations, vert_dilate)
        horizontal_lines_img = Box.get_horizontal_lines(img, kernel_length, iterations)

        if vert_flg:
            vertical_lines_img = Box.get_hough_lines(vertical_lines_img)
        if horz_flg:
            horizontal_lines_img = Box.get_hough_lines(horizontal_lines_img)
        if join_lines:
            horizontal_lines_img = join_consecutive_lines(horizontal_lines_img, join_contour_count)

        img_final_bin = cv2.bitwise_or(vertical_lines_img, horizontal_lines_img)
        return img_final_bin

    @staticmethod
    def get_hough_lines(img):
        kernel_length = np.array(img).shape[1] // 80
        lines = cv2.HoughLinesP(image=img, rho=0.05, theta=np.pi / 600, threshold=10, lines=np.array([]),
                                minLineLength=kernel_length//4, maxLineGap=kernel_length*2)

        if not isinstance(lines, type(None)):
            a, b, c = lines.shape
            for i in range(a):
                cv2.line(img, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), 255, thickness=1)
        return img
