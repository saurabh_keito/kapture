"""ocr_func
* @author: Rajendraprasad Lawate
* Copyright (C) 2019 Keito Tech Private Ltd. - All Rights Reserved
* This File's function are supporting functions dfor OCR.
"""

import numpy as np
from .ocr import BoxData


def get_key_value(final_boxes, check_boxes=None, allowed_lower_row_diff=-0.8, allowed_higher_row_diff=0.8,
                  allowed_lower_space_diff=-5, allowed_higher_space_diff=5, **kwrgs):
    """
    Manually detects /t and /n in the provided text through co-ordinates
    :param final_boxes: BoxData.WordParam object containing words and their co-ordinates
    :param check_boxes: all checkbox list (if any)
    :param allowed_lower_row_diff:
    :param allowed_higher_row_diff:
    :param allowed_lower_space_diff:
    :param allowed_higher_space_diff:
    :return: list of word objects and \t or \n
    """
    new_text = ""
    key_value_coord = []
    char_spacing_list = []
    for i in final_boxes:
        char_spacing_list.append((final_boxes[i].xmax - final_boxes[i].xmin) / len(final_boxes[i].word))
    if final_boxes:
        char_spacing = np.average(char_spacing_list)
        new_text += final_boxes[0].word
        key_value_coord.append(final_boxes[0])

        for i in range(1, len(final_boxes)):
            space_diff = (final_boxes[i].xmin - final_boxes[i - 1].xmax) / char_spacing
            row_diff = (final_boxes[i].ymax - final_boxes[i - 1].ymax) / char_spacing

            if -0.8 < row_diff > 0.8:
                new_text += "\n"
                key_value_coord.append("\n")

            elif space_diff > 2:
                new_text += "\t"
                key_value_coord.append("\t")
            new_text += " " + final_boxes[i].word
            key_value_coord.append(final_boxes[i])

        if check_boxes:
            key_value_coord = get_keys_with_check_boxes(key_value_coord, check_boxes, char_spacing,
                                                        allowed_lower_row_diff, allowed_higher_row_diff,
                                                        allowed_lower_space_diff, allowed_higher_space_diff)

    return key_value_coord

# TODO: check if returnCoords is used anywhere else remove it


def return_coordinates(k, final_boxes):
    final_boxes_copy = final_boxes.copy()
    tempxmin = []
    tempxmax = []
    tempymax = []
    tempymin = []

    for l in k.split(" "):
        for m in final_boxes_copy:
            if final_boxes_copy[m]['word'] == l and m in final_boxes:
                tempxmin.append(final_boxes[m]['xmin'])
                tempymin.append(final_boxes[m]['ymin'])
                tempxmax.append(final_boxes[m]['xmax'])
                tempymax.append(final_boxes[m]['ymax'])
                del final_boxes[m]
                break
    if not (tempxmin or tempymin or tempxmax or tempymax):
        return None
    else:
        return {"word": k, "xmin": min(tempxmin), "xmax": max(tempxmax),
                "ymin": min(tempymin), "ymax": max(tempymax)}


def get_keys_with_check_boxes(key_value_coord, check_boxes, char_spacing, allowed_lower_row_diff=-0.8,
                              allowed_higher_row_diff=0.8, allowed_lower_space_diff=-5, allowed_higher_space_diff=5,**kwrgs):
    key_value_coord1 = key_value_coord.copy()
    for i in check_boxes:
        current_check_box = check_boxes[i]
        nearest_words = [[], [], [], []]
        for j in key_value_coord:

            if isinstance(j, BoxData.WordParams):
                space_diff = (j.xmin - current_check_box.xmax) / char_spacing
                row_diff = (j.ymax - current_check_box.ymax) / char_spacing
                if allowed_lower_row_diff < row_diff < allowed_higher_row_diff and \
                        allowed_lower_space_diff < space_diff < allowed_higher_space_diff: # Default
                # if -0.8 < row_diff < 1.0 and -5 < space_diff < 15: # Changed for Dental
                    nearest_words[0].append(j.xmin - current_check_box.xmax)
                    nearest_words[1].append(j.word)
                    nearest_words[2].append(current_check_box.value)
                    nearest_words[3].append(j)

        sorted_index = list(np.argsort(nearest_words[0]))

        nearest_words1 = [[nearest_words[0][x], nearest_words[1][x],
                           nearest_words[2][x], nearest_words[3][x]]
                          for x in sorted_index]
        if nearest_words1:
            index_check = key_value_coord1.index(nearest_words1[0][3])
            key_value_coord1.insert(index_check, "[checkbox " + str(nearest_words1[0][2]) + "]")
    return key_value_coord1
