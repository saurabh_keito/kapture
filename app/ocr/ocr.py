#  * Copyright (C) 2019  Keito Tech Private Ltd. - All Rights Reserved
#  * @author: Aditya Aman, Rajendraprasad Lawate

from ..ocr.boxdet import Box, box_detection, sort_contours
from PIL import Image
import pytesseract
import numpy as np
import pandas as pd
import time
from Kapture.app.utilities import CONSTANTS
import cv2
import re
import json
from Kapture.app.utilities.extension_helper import correct_skew

BOX_FLAG = 'box_flg'


class BoxData:

    def __init__(self, box_params):
        self.box_params = box_params
        self.text = ""
        self.word_params = {}
        self.img = []

    class WordParams:
        def __init__(self, word_id, word, xmin, ymin, xmax, ymax):
            self.word_id = word_id
            self.word = word
            self.xmin = xmin
            self.ymin = ymin
            self.xmax = xmax
            self.ymax = ymax
            self.centroid_x = int(xmin + ((xmax - xmin) / 2))
            self.centroid_y = int(ymin + ((ymax - ymin) / 2))

        def __str__(self):
            data = 'WordParm, {}, {}, {}, {}, {}'.format(self.xmin, self.ymin, self.xmax, self.ymax, self.centroid_x,
                                                         self.centroid_y, self.word)
            return data

        def get_type(self):
            return 0, self.xmin, self.ymin, self.xmax, self.ymax, self.word, self.word_id


class KeitoOcr:

    def __init__(self, image_path, key_path, manager, config_path=None):
        self.lock = manager.Lock()
        setattr(CONSTANTS, "config", self.Config(config_path))
        self.img_path = image_path
        self.img_color = cv2.imread(image_path)
        if hasattr(CONSTANTS.config, 'skew_correction'):
            if CONSTANTS.config.skew_correction:
                self.img_color = correct_skew(self.img_color)
        self.img_gray = cv2.cvtColor(self.img_color.copy(), cv2.COLOR_BGR2GRAY)
        if hasattr(CONSTANTS.config, 'binary_method'):
            if CONSTANTS.config.binary_method == 'thresh':
                (t, self.img_binary) = cv2.threshold(self.img_gray, CONSTANTS.config.binary_thresh, 255, cv2.THRESH_BINARY)
        else:
            self.img_binary = cv2.adaptiveThreshold(
                self.img_gray.copy(),
                255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                cv2.THRESH_BINARY, 15, 4
            )

        if CONSTANTS.config.box_detection[BOX_FLAG]:
            self.boxes = box_detection.Box.get_boxes(self.img_binary, **CONSTANTS.config.box_detection)
        self.final_boxes = manager.dict()
        self.height, self.width = self.img_gray.shape
        self.final_key_value = manager.dict()
        self.raw = pd.read_excel(key_path)
        self.RC = manager.list(self.raw.RC)
        self.RL = manager.list(self.raw.RL)
        self.all_keys = manager.list(self.raw.KEYS)
        self.all_keys1 = manager.list(self.raw.KEYS)
        self.all_keys_original = manager.list(self.raw.KEYS)
        self.key_flags = manager.list(self.raw.BOXFLAG)
        self.key_max_words = manager.list(self.raw.MAXWORDS)
        self.key_max_words1 = manager.list(self.raw.MAXWORDS)
        self.display_keys = manager.list(self.raw.DISPLAYKEYS)
        self.bottom_values = manager.list()
        self.tables = manager.list()
        self.table_new = manager.list()
        self.symbol = manager.list()
        self.symbol.extend(['.', '*', '|', '+', '?', '{', '}', ',', '=', '#', '(', ')', '[', ']', '-', '^', '$', '!', '<', '>'])

    class Config:

        def __init__(self, json_path=None):
            if json_path is None:
                json_path = CONSTANTS.default_config
            params = json.load(open(json_path, 'r'))
            for i in params:
                setattr(self, i, params[i])

    @staticmethod
    def get_key_x(box):
        return box.centroid_x

    @staticmethod
    def get_key_y(box):
        return box.centroid_y

    def place_check_boxes(self, i, temp, orig_keys, keys=None):
        """
        :param i: box id
        :param temp: list of word objects, \n or \t, and check box values
        :param orig_keys: orig keys
        :param keys: detected keys
        :return: None - saves key values in self object
        """
        if temp[0] and orig_keys:
            temp_value = ""
            if len(orig_keys) == 1:
                if len(temp) > 1:
                    for z in range(1, len(temp)):
                        for zz in temp[z]:
                            temp_value += " " + zz
                self.final_key_value[i] = {
                    "key": orig_keys[0], "value": temp_value
                }
            elif keys:
                self.lock.acquire()
                if i in self.final_key_value:
                    del self.final_key_value[i]
                self.lock.release()
                text = ""
                for x in temp:
                    text += " " + " ".join([y for y in x])
                for z in range(len(keys)):
                    if z == len(keys) - 1:
                        new_text = text.split(keys[z])[1]
                        self.final_boxes[i + (z + 1) / 10] = self.final_boxes[i]
                        self.final_key_value[i + (z + 1) / 10] = {"key": orig_keys[z], "value": new_text}
                    else:
                        str1 = keys[z]
                        str2 = keys[z + 1]
                        for alpha in str1:
                            if alpha in self.symbol:
                                str1 = str1.replace(alpha, '\\' + alpha)
                        for alpha in str2:
                            if alpha in self.symbol:
                                str2 = str2.replace(alpha, '\\' + alpha)
                        expression = str1 + "(.*)" + str2
                        result = re.search(expression, text)
                        if result:
                            new_text = result.group(1)
                            self.final_boxes[i + (z + 1) / 10] = self.final_boxes[i]
                            self.final_key_value[i + (z + 1) / 10] = {
                                "key": orig_keys[z], "value": new_text
                            }

    def get_adjacent_boxes(self, box, flg=False):
        """
        :param box: The Box from which adjacent boxes to get
        :param flg: set flg True for below Boxes or False for right Boxes
        :return: List of Ids of Boxes
        """
        if flg:
            boxes_below = []
            boxesCentroidBelow = []
            for j in self.boxes:
                c1 = box.xmin < self.boxes[j].centroid_x < box.xmax
                c2 = box.ymax < self.boxes[j].centroid_y < self.height
                if j != box.box_id and c1 and c2:
                    boxes_below.append(self.boxes[j])
            if boxes_below:
                return [x.box_id for x in boxes_below]
        else:
            boxes_right = []
            for j in self.boxes:
                if j != box.box_id and (box.ymin < self.boxes[j].centroid_y < box.ymax) and (
                        box.xmax < self.boxes[j].centroid_x < self.width):
                    boxes_right.append(self.boxes[j])

            if boxes_right:
                return [
                    x.box_id for x in sorted(boxes_right, key=self.get_key_x)
                ]
        return []

    def get_key_value_by_flag(self, box: Box, text, keys, orig_keys, im_pil=None):
        """
        :param box: Box object of current box
        :param text: raw text
        :param keys: detected keys
        :param orig_keys: orig keys from excel
        :param im_pil: PIL image
        :return: None - updates key and value acc to flags in excel
        """

        if keys:
            for z in range(len(keys)):
                if orig_keys[z] in self.all_keys:
                    key_ind = self.all_keys.index(orig_keys[z])
                    if self.key_flags[key_ind] == "W":
                        text = text.replace("\n", " ")
                        text = " ".join([s for s in text.split()])
                        if z == len(keys) - 1:
                            new_text = text.split(keys[z])[1]
                            self.final_key_value[box.box_id] = {"key": orig_keys[z], "value": new_text}
                        else:
                            str1 = keys[z]
                            str2 = keys[z + 1]
                            for alpha in str1:
                                if alpha in self.symbol:
                                    str1 = str1.replace(alpha, '\\' + alpha)
                            for alpha in str2:
                                if alpha in self.symbol:
                                    str2 = str2.replace(alpha, '\\' + alpha)

                            expression = str1 + "(.*)" + str2
                            # To avoid sre_constants.error: missing ),
                            # unterminated subpattern
                            result = re.search(expression, text)
                            if result:
                                new_text = result.group(1)
                                self.final_boxes[box.box_id + (z+1) / 10] = self.final_boxes[box.box_id]
                                self.final_key_value[
                                    box.box_id + (z+1) / 10] = {"key": orig_keys[z], "value": new_text}
                    elif self.key_flags[key_ind] == "B":
                        boxes_below = []
                        boxes_centroid_below = []
                        for j in self.boxes:
                            if j != box.box_id and (box.xmin < self.boxes[j].centroid_x < box.xmax) and (
                                    box.ymax < self.boxes[j].centroid_y < self.height):
                                boxes_below.append(j)
                                boxes_centroid_below.append(self.boxes[j].centroid_y)
                        if boxes_centroid_below:
                            closest_box = boxes_below[boxes_centroid_below.index(min(boxes_centroid_below))]
                            self.bottom_values.append([box.box_id, orig_keys[z], [closest_box]])
                        else:
                            self.final_key_value[box.box_id] = {"key": orig_keys[z], "value": ""}
                    elif self.key_flags[key_ind] == "BX":
                        boxes_below = []
                        boxes_centroid_below = []
                        for j in self.boxes:
                            c1 = box.xmin < self.boxes[j].centroid_x < box.xmax
                            c2 = box.ymax < self.boxes[j].centroid_y < self.height
                            if j != box.box_id and c1 and c2:
                                boxes_below.append(j)
                                boxes_centroid_below.append(self.boxes[j].centroid_y)
                        if boxes_below:
                            closest_boxes = boxes_below[0:min(6, len(boxes_centroid_below))]
                            self.bottom_values.append([box.box_id, orig_keys[z], closest_boxes])
                    elif self.key_flags[key_ind] == "WX":
                        text = pytesseract.image_to_string(im_pil, config="--oem 3 --psm 6")
                        text = text.replace("\n", " ")
                        text = " ".join([s for s in text.split()])
                        new_text = text.replace(keys[z], "")
                        self.final_key_value[box.box_id] = {"key": orig_keys[z], "value": new_text}
                    elif self.key_flags[key_ind] == "BR":
                        boxes_below = []
                        boxes_centroid_below = []
                        for j in self.boxes:
                            if j != box.box_id and (box.xmin < self.boxes[j].centroid_x < box.xmax) and (
                                    box.ymax < self.boxes[j].centroid_y < box.ymax + self.height//5) and 1.1 > (
                                    self.boxes[j].xmax - self.boxes[j].xmin) / (
                                    self.boxes[box.box_id].xmax - self.boxes[box.box_id].xmin) > 0.9:
                                boxes_below.append(j)
                                boxes_centroid_below.append((self.boxes[j].centroid_x, self.boxes[j].centroid_y))
                        if boxes_centroid_below:
                            self.bottom_values.append([box.box_id, orig_keys[z], boxes_below])
                        else:
                            self.final_key_value[box.box_id] = {"key": keys[z], "value": ""}
                    elif self.key_flags[key_ind] == "B X PRI" or self.key_flags[key_ind] == "B X PER":
                        boxes_below = []
                        boxes_centroid_below = []
                        for j in self.boxes:
                            c1 = box.xmin < self.boxes[j].centroid_x < box.xmax
                            c2 = box.ymax < self.boxes[j].centroid_y < self.height
                            c3 = 1.1 > (self.boxes[j].xmax - self.boxes[j].xmin) / (
                                    (self.boxes[box.box_id].xmax - self.boxes[box.box_id].xmin) // 2) > 0.9
                            if j != box.box_id and c1 and c2 and c3:
                                boxes_below.append(j)
                                boxes_centroid_below.append(self.boxes[j].centroid_y)
                        if boxes_below:
                            self.bottom_values.append([box.box_id, orig_keys[z], boxes_below])
                        else:
                            self.final_key_value[box.box_id] = {"key": orig_keys[z], "value": ""}
                    elif self.key_flags[key_ind] == "R":
                        boxes_below = self.get_adjacent_boxes(box)
                        if boxes_below:
                            self.bottom_values.append([box.box_id, orig_keys[z], [boxes_below[0]]])
                        else:
                            self.final_key_value[box.box_id] = {"key": orig_keys[z], "value": ""}
                    elif self.key_flags[key_ind] == "XS":
                        temp = self.RC[key_ind].split(",")
                        row = int(temp[0])
                        col = int(temp[1])
                        boxes_below = []
                        right_boxes = [box.box_id]
                        tp = self.get_adjacent_boxes(box, flg=False)
                        right_boxes.extend(tp[0:col])
                        for x in right_boxes:
                            boxes_below.append(
                                self.get_adjacent_boxes(self.boxes[x], flg=True)[0:row])
                        self.tables.append({"rows": boxes_below,
                                            "columns": right_boxes})
                    elif self.key_flags[key_ind] == "XS1":
                        temp = self.RC[key_ind].split(",")
                        row = int(temp[0])
                        col = int(temp[1])
                        boxes_below = []
                        right_boxes = []
                        tp = self.get_adjacent_boxes(box, flg=False)
                        right_boxes.extend(tp[0:col])
                        boxes_below.extend(self.get_adjacent_boxes(box, flg=True)[0:row])
                        self.table_new.append([box.box_id, boxes_below, right_boxes])
                    elif self.key_flags[key_ind] == 'RXK':
                        right_boxes = self.get_adjacent_boxes(box)
                        right_keys = self.RL[key_ind].split(',')
                        for i in range(min(len(right_keys), len(right_boxes))):
                            self.bottom_values.append(
                                [right_boxes[i], orig_keys[z] + '-' + right_keys[i], [right_boxes[i]]])
                    elif self.key_flags[key_ind].isalnum():
                        first_char = self.key_flags[key_ind][0]
                        if first_char == 'B' or first_char == "R":
                            regex = r"^B[\d*]" if first_char == 'B' else r"^R[\d*]"
                            matches = re.finditer(regex, self.key_flags[key_ind], re.MULTILINE)
                            for match in matches:
                                if str(match.start()):
                                    boxes_count = self.key_flags[key_ind].split(first_char)[1]
                                    below_boxes_count = int(boxes_count)
                                    boxes_right = self.get_adjacent_boxes(box, True if first_char == 'B' else False)

                                    if boxes_right:
                                        closest_right_boxes = boxes_right[0:min(below_boxes_count, len(boxes_right))]
                                        self.bottom_values.append([box.box_id, orig_keys[z], closest_right_boxes])

                        elif self.key_flags[key_ind][0] == 'W':
                            boxes_count = self.key_flags[key_ind].split('WB')[1].split('R')
                            below_boxes_count = int(boxes_count[0])
                            right_boxes_count = int(boxes_count[1])

                            flag_type2 = None
                            if box.area < 35000:
                                flag_type2 = True
                                self_box = []
                                below_boxes_count = below_boxes_count + 1
                            else:
                                self_box = [box.box_id]
                            boxes_below = self.get_adjacent_boxes(box, True)
                            closest_below_boxes = []
                            if boxes_below:
                                closest_below_boxes = boxes_below[0:min(below_boxes_count, len(boxes_below))]

                            if flag_type2:
                                temp_box_id = self.boxes[closest_below_boxes[0]].box_id
                            else:
                                temp_box_id = box.box_id

                            boxes_right = self.get_adjacent_boxes(self.boxes[temp_box_id])
                            closest_right_boxes = []

                            if boxes_right:
                                closest_right_boxes = boxes_right[0:min(right_boxes_count, len(boxes_right))]

                            if closest_below_boxes or closest_right_boxes:
                                self.bottom_values.append([box.box_id, orig_keys[z],
                                                           self_box + closest_below_boxes + closest_right_boxes])

                    self.delete_keys(key_ind)

    def delete_keys(self, key_ind):
        """
        * Acquires the lock and deletes the row from Excel
        * The excel is mapped on different variables
        :param key_ind: The row number of the excel
        :return: Void function
        """
        self.lock.acquire()
        del self.key_flags[key_ind]
        del self.all_keys[key_ind]
        del self.key_max_words[key_ind]
        del self.RC[key_ind]
        del self.RL[key_ind]
        self.lock.release()

    @staticmethod
    def process_check_boxes(inside_boxes, binary_img, cropped_img_gray, box,
                            lower_limit, upper_limit, expand_x, expand_y, check_threshold, **kwargs):
        """
        This function processes checkboxes to calculate if checkbox has
        value true or false as well as clearing checkbox area
        for further text extraction
        :param inside_boxes: detected checkbox Box objects
        :param binary_img: binary image to check value of checkbox by density
        :param cropped_img_gray: cropped grays-cale image
        :param box: box object of Original big box
        :param expand_x: % to expand horizontally and make checkbox area white
        :param expand_y: % to expand vertically and make checkbox area white
        :param lower_limit: min ratio of width to height ratio
        :param upper_limit: max ratio of width to height ratio
        :param check_threshold: Threshold limit to mark checkbox as true
        :return: cropped_img_gray with checkbox removed, all_checkboxes containing box object with boolean value.
        """
        x_min = box.xmin
        y_min = box.ymin
        all_checkboxes = {}
        for j in inside_boxes:
            xmin1 = inside_boxes[j].xmin
            ymin1 = inside_boxes[j].ymin
            xmax1 = inside_boxes[j].xmax
            ymax1 = inside_boxes[j].ymax
            if upper_limit > abs(ymax1 - ymin1) / abs(xmax1 - xmin1) > lower_limit:
                check_box = (~binary_img[y_min + ymin1:y_min + ymax1, x_min + xmin1: x_min + xmax1]) / 255
                check_box1 = cropped_img_gray[ymin1:ymax1, xmin1:xmax1]
                text = pytesseract.image_to_string(Image.fromarray(check_box1), config="--psm 10")
                all_checkboxes[j] = inside_boxes[j]
                all_checkboxes[j].xmin += x_min
                all_checkboxes[j].ymin += y_min
                all_checkboxes[j].xmax += x_min
                all_checkboxes[j].ymax += y_min
                if check_box.sum() / (check_box.shape[0] * check_box.shape[1]) > check_threshold or 'x' in text.lower():
                    all_checkboxes[j].value = True
                else:
                    all_checkboxes[j].value = False

                new_ymin = ymin1 - int((ymax1 - ymin1) * expand_y)
                new_ymin = new_ymin if new_ymin >= 0 else 0
                new_xmin = xmin1 - int((xmax1 - xmin1) * expand_x)
                new_xmin = new_xmin if new_xmin >= 0 else 0
                new_ymax = ymax1 + int((ymax1 - ymin1) * expand_y)
                new_ymax = cropped_img_gray.shape[0] if new_ymax > cropped_img_gray.shape[0] else new_ymax
                new_xmax = xmax1 + int((xmax1 - xmin1) * expand_x)
                new_xmax = cropped_img_gray.shape[1] if new_xmax > cropped_img_gray.shape[1] else new_xmax
                cropped_img_gray[new_ymin:new_ymax, new_xmin:new_xmax] = 255
        return cropped_img_gray, all_checkboxes

    @staticmethod
    def process_radio_button(inside_boxes, binary_img, cropped_img_gray, box,
                             expand_x, expand_y, check_threshold, **kwargs):
        """
        This function processes radio button to calculate if radio button has
        value true or false as well as clearing radio button area
        for further text extraction
        :param inside_boxes: detected radio button Box objects
        :param binary_img: binary image to check value of radio button by density
        :param cropped_img_gray: cropped gray-scale image
        :param box: box object of Original big box
        :param expand_x: % to expand horizontally and make radio button area white
        :param expand_y: % to expand vertically and make radio button area white
        :param check_threshold: Threshold limit to mark radio button as true
        :return: cropped_img_gray with radio button removed, all_checkboxes containing box object with boolean value.
        """
        x_min = box.xmin
        y_min = box.ymin
        all_checkboxes = {}
        for j in inside_boxes:
            xmin1 = inside_boxes[j].xmin
            ymin1 = inside_boxes[j].ymin
            xmax1 = inside_boxes[j].xmax
            ymax1 = inside_boxes[j].ymax
            check_box = (~binary_img[y_min + ymin1:y_min + ymax1, x_min + xmin1: x_min + xmax1]) / 255
            all_checkboxes[j] = inside_boxes[j]
            all_checkboxes[j].xmin += x_min
            all_checkboxes[j].ymin += y_min
            all_checkboxes[j].xmax += x_min
            all_checkboxes[j].ymax += y_min
            if check_box.sum() / (check_box.shape[0] * check_box.shape[1]) > check_threshold:
                all_checkboxes[j].value = True
            else:
                all_checkboxes[j].value = False
            new_ymin = ymin1 - int((ymax1 - ymin1) * expand_y)
            new_ymin = new_ymin if new_ymin >= 0 else 0
            new_xmin = xmin1 - int((xmax1 - xmin1) * expand_x)
            new_xmin = new_xmin if new_xmin >= 0 else 0
            new_ymax = ymax1 + int((ymax1 - ymin1) * expand_y)
            new_ymax = cropped_img_gray.shape[0] if new_ymax > cropped_img_gray.shape[0] else new_ymax
            new_xmax = xmax1 + int((xmax1 - xmin1) * expand_x)
            new_xmax = cropped_img_gray.shape[1] if new_xmax > cropped_img_gray.shape[1] else new_xmax
            cropped_img_gray[new_ymin:new_ymax, new_xmin:new_xmax] = 255

        return cropped_img_gray, all_checkboxes

    def get_final_key_and_value(self):
        """
        Function converts final key value dictionary into 3 lists final_keys, final_values, final_coords
        :return: final_keys, final_values, final_coords
        """
        final_keys = []
        final_values = []
        final_coords = []
        x = self.final_key_value.copy()
        sorted_final_keys = sorted(x)
        for i in sorted_final_keys:
            final_coords.append(
                [
                    self.final_boxes[i].box_params.xmin,
                    self.final_boxes[i].box_params.ymin,
                    self.final_boxes[i].box_params.xmax,
                    self.final_boxes[i].box_params.ymax
                ]
            )
            final_keys.append(self.final_key_value[i]['key'])
            final_values.append(self.final_key_value[i]['value'])
        return final_keys, final_values, final_coords

    def get_new_image(self, i):
        """
        This function removes other boxes and returns only cropped and masked image
        :param i: box id
        :return: cropped_img_gray, cropped_img_binary
        """
        box_id = i
        self.final_boxes[box_id] = BoxData(self.boxes[i])
        xmin = self.boxes[i].xmin
        ymin = self.boxes[i].ymin
        xmax = self.boxes[i].xmax
        ymax = self.boxes[i].ymax
        new_img = np.zeros((self.height, self.width), dtype=np.uint8)
        new_img[ymin:ymax, xmin:xmax] = 255
        clean_mask = []
        for j in self.boxes:
            xmin1 = self.boxes[j].xmin
            ymin1 = self.boxes[j].ymin
            xmax1 = self.boxes[j].xmax
            ymax1 = self.boxes[j].ymax
            if j != i:
                if (xmin < xmin1 < xmax and ymin < ymin1 < ymax) or (xmin < xmax1 < xmax and ymin < ymax1 < ymax) or \
                        (xmin < xmin1 < xmax and ymin < ymax1 < ymax) or (xmin < xmax1 < xmax and ymin < ymin1 < ymax):
                    clean_mask.append(j)
        img_gray = self.img_gray.copy()
        img_binary = self.img_binary.copy()
        for x in clean_mask:
            xmin1 = self.boxes[x].xmin
            ymin1 = self.boxes[x].ymin
            xmax1 = self.boxes[x].xmax
            ymax1 = self.boxes[x].ymax

            if hasattr(CONSTANTS.config, 'area_mask_color'):
                if CONSTANTS.config.area_mask_color:
                    img_gray[ymin1:ymax1, xmin1:xmax1] = 255
                    img_binary[ymin1:ymax1, xmin1:xmax1] = 255
                else:
                    img_gray[ymin1:ymax1, xmin1:xmax1] = 0
                    img_binary[ymin1:ymax1, xmin1:xmax1] = 0
            else:
                img_gray[ymin1:ymax1, xmin1:xmax1] = 255
                img_binary[ymin1:ymax1, xmin1:xmax1] = 255

        cropped_img_gray = img_gray[ymin:ymax, xmin:xmax]
        cropped_img_binary = img_binary[ymin:ymax, xmin:xmax]
        return cropped_img_gray, cropped_img_binary

    @staticmethod
    def get_contour_text(img):
        """
        Used for detecting characters by contouring(Used for dental forms)
        :param img: input image
        :return: list of detected text
        """
        img[img > 200] = 255
        img[img < 100] = 0
        img1 = img.copy()
        img = ~img

        kernel = np.ones((3, 3))
        img = cv2.dilate(img, kernel, iterations=2)

        contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        (contours, boundingBoxes) = sort_contours(contours, method="left-to-right")
        result = []
        counter = 0
        for c in contours:
            if hierarchy[0][counter][2] == -1:  # Take those contours who do not have child
                x, y, w, h = cv2.boundingRect(c)
                new_img = img1[y:y + h, x:x + w]
                im_pil = Image.fromarray(new_img)
                text = pytesseract.image_to_string(im_pil, config="--oem 3 --psm 10")
                if text != "":
                    result.append(text)
            counter += 1
        return result

    @staticmethod
    def get_excel_path(final_keys, final_values):
        """
        saves excel
        :param final_keys: keys
        :param final_values: values
        :return: saved excel path
        """
        df = pd.DataFrame([final_keys, final_values]).transpose()
        df.columns = ["keys", "values"]
        excel_file_path = CONSTANTS.savePath + "/" + str(time.time()) + ".xlsx"
        df.to_excel(excel_file_path)
        return excel_file_path

    def update_image(self,image_path):
        self.img_path = image_path
        self.img_color = cv2.imread(image_path)
        self.img_gray = cv2.cvtColor(self.img_color.copy(), cv2.COLOR_BGR2GRAY)
        self.img_binary = cv2.adaptiveThreshold(
            self.img_gray.copy(),
            255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            cv2.THRESH_BINARY, 15, 4
        )
        self.boxes = box_detection.Box.get_boxes(self.img_binary, **CONSTANTS.config.box_detection)
        # self.img, self.boxes = getBoxes(self.img, vertFlg=False, horzFlg=True, joinLines=True)
        # self.boxes = removeDuplicateBoxes(self.boxes)
        # self.boxes = box_helpers.remove_small_boxes(self.boxes)
        self.height, self.width = self.img_gray.shape
        self.final_key_value.clear()
        self.tables[:] = []
        self.bottom_values[:] = []

    def get_display_keys(self, final_keys):
        """
        This function will provide alternative display keys to original keys.
        :param final_keys: list of original keys
        :return: list of display keys
        """
        final_keys_cpy = final_keys.copy()
        for key in final_keys:
            try:
                index = self.all_keys_original.index(key)
                if str(self.display_keys[index]) != 'nan':
                    final_keys_cpy[final_keys_cpy.index(key)] = self.display_keys[index]
                    self.all_keys_original.pop(index)
                    self.display_keys.pop(index)
            except:
                pass

        return final_keys_cpy
